import * as express from "express";
import * as cors from "cors";
import { createServer } from "http";
import { Server } from "net";

const PORT = process.env["PORT"] || 3000;

const app = express();
app.set("port", PORT);
app.use(cors());

app.get("/", (req, res) => {
  res.send("Hello World")
});

const server = createServer(app);

export function startServer(): Server {
  return server.listen(PORT, () => {
    console.log("server listen on port ", PORT);
  });
}
