"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const cors = require("cors");
const http_1 = require("http");
const PORT = process.env["PORT"] || 3000;
const app = express();
app.set("port", PORT);
app.use(cors());
app.get("/", (req, res) => {
    res.send("Hello World");
});
const server = http_1.createServer(app);
function startServer() {
    return server.listen(PORT, () => {
        console.log("server listen on port ", PORT);
    });
}
exports.startServer = startServer;
