"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("./server");
async function startApp() {
    server_1.startServer();
}
startApp();
